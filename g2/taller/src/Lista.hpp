//Uriel Ivan Mischener Forte
//LU 146|19
#include "Lista.h"

Lista::Lista() {
    longitud_ = 0;
    primero_ = nullptr;
    ultimo_ = nullptr;
}

Lista::Lista(const Lista& l) : Lista() {
    //Inicializa una lista vacía y luego utiliza operator= para no duplicar el código de la copia de una lista.
    *this = l;
}

Lista::~Lista() {
    int tot = longitud_;
    for(int i = tot-1; i > 0; i--){
        eliminar(i);
    }
    delete primero_;
    //delete ultimo_;
}

Lista& Lista::operator=(const Lista& aCopiar) {
    // Completar
    int longi = longitud_;
    for(int i = longi-1; i >= 0; i--){
        this->eliminar(i);
    }
    int elem = aCopiar.longitud();
    for(int i = 0; i < elem; i++){
        this->agregarAtras(aCopiar.iesimo(i));
    }
    return *this;
}

void Lista::agregarAdelante(const int& elem) {
    Nodo* n = new Nodo();
    n->valor_ = elem;
    if (longitud_ == 0) {
        primero_ = n;
        ultimo_ = n;
        // dejar anterior y siguiente en null
    } else {
        //n->anterior_ = ultimo_;
        // siguiente queda en null
        primero_ -> anterior_ = n;
        n ->siguiente_ = primero_;
        primero_ = n;

    }
    longitud_++;
    //cout << ultimo_->valor_;
}

void Lista::agregarAtras(const int& elem) {
    if(longitud_ !=0) {
        longitud_++;
        Nodo* n = new Nodo();
        n->valor_ = elem;
        n->anterior_ = this->ultimo_;
        this->ultimo_->siguiente_ = n;
        this->ultimo_ = n;
    }else{
        longitud_++;
        Nodo* n = new Nodo();
        n->valor_ = elem;
        this->primero_ = n;
        this->ultimo_ = n;
    }
    //cout << ultimo_->valor_;
}

void Lista::eliminar(Nat i) {
    // Completar
    longitud_--;
    if(i != 0 && i != this->longitud()) {
        Nodo *n = this->primero_;
        for (int j = 0; j < i; j++) {
            n = n->siguiente_;
        }
        n->siguiente_->anterior_ = n->anterior_;
        n->anterior_->siguiente_ = n->siguiente_;
        delete n;
    }else if(i == 0){
        if(longitud_!=0) {
            Nodo *n = this->primero_->siguiente_;
            delete primero_;
            primero_ = n;
        }else{
            delete primero_;
            primero_ = nullptr;
            ultimo_ = nullptr;
        }
    }else if(i == this->longitud()){
        if(longitud_!=0) {
            Nodo *n = this->ultimo_->anterior_;
            delete ultimo_;
            ultimo_ = n;
        }else {
            delete primero_;
            primero_ = nullptr;
            ultimo_ = nullptr;
        }
    }
}

int Lista::longitud() const {
    // Completar
    return longitud_;
}

const int& Lista::iesimo(Nat i) const {
    Nodo* n = primero_;
    //cout << primero_->valor_ + 1 <<" el valor en el primer lugar"<<endl;
    //cout << n->valor_;
    for(int j = 0; j < i; j++){
        //cout << "i";
        n = n->siguiente_;
    }
    return n->valor_;
    //assert(false);
}

int& Lista::iesimo(Nat i) {
    Nodo* n = primero_;
    //cout << primero_->valor_;
    //cout << n->valor_;
    for(int j = 0; j < i; j++){
        //cout << "i";
        n = n->siguiente_;
    }
    return n->valor_;
    //assert(false);
}

void Lista::mostrar(ostream& o) {
    // Completar
}
